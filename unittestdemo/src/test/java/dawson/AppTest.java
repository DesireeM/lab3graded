package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void shouldReturnX()
    {
        assertEquals("testing echo method to see if it returns the right number", 5, App.echo(5));
    }

    @Test
    public void shouldReturn1MoreThanX()
    {
        assertEquals("testing oneMore method to make sure it adds 1 more than x", 6, App.oneMore(5));
    }
}
